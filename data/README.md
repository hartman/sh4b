# README

The reconstruction tags changed between R20 and now, but according to this [jira](https://its.cern.ch/jira/browse/ATLMCPROD-10230), the r-tags for the mc20 reprocessing campaign are:

- mc20a: r13167
- mc20d: r13144
- mc20e: r13145

**Ntuples from Dari:**

New ntuples with .root files for ttbar and dijets (3 slices are missing because derivations of daod_phys p5511 are not finished). They are in the rucio with this identifier: `user.dabattul.ntup_bkg_24022023.*_TREE`. They are produced with AthAnalysis, 22.2.108.

SH4b signals: `user.dabattul.ntup_SH4b_AF3_24022023.*_TREE`

Run 2 data: `user.dabattul.ntup_data_24022023.*_TREE`.


